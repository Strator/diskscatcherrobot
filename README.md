# DisksCatcherRobot

Robot con sistema di sterzata Ackerman, che dato un grafo prefissato dei percorsi, cerca dei dischi da posizionare negli appositi stalli

## Requisiti
- Caratteristiche Robot:
    - Modello : Ackeerman steering
    - Massa: 20kg
    - Coefficiente Lineare di frizione: 0.7
    - Distanza ruote: 20cm
    - Raggio ruote: 3cm
    - Serbatoio: 3 dischi
    - Sensori: Sensore Colore

- Caratteristiche Ambiente:
    - Paths : Grafo prefissato
    - Numero Dischi: 10
    - Numero Stazioni: 20
    - Numero Garage: 3
    - Tipologie dischi: 3 (Rossi, Verdi e Blu)
    - Tipologie vertici: 3 (Normali, Stazioni e Garage)
    - Algoritmo di Navigazione: Percorso minimo calcolato con Djikstra

- Obiettivo:
Scansione delle stazioni e cattura dei dischi incontrati con priorità Rosso, Blu, Verde. Scarica negli appositi garage divisi per colore. La scarica avviene quando il serbatoio è pieno o sono finiti i dischi. 

Per maggiori dettagli:

[traccia](pdf/Testo.pdf)

## Istruzioni di avvio
- 1. avviare xlaunch
- 2. controllare il file .bashrc che contenga l'ip corretto per lo schermo
- 3. eseguire il comando:  
```
./test.sh
```

lo script test.sh avvia due processi python il primo di occupa della grafica il secondo del reasoning tramite phidias, essi comunicano tramite 
server http

## Project status
Completed
