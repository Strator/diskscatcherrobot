from cmath import inf
from fibheap import Node

class Vertex(Node):
    def __init__(self,label,x=None,y=None,key=inf):
        super().__init__(key)
        self.label = label
        self.x = x
        self.y = y
        self.disk = None
        self.type = "normal" # normal | station | garage | start
        self.level = 0

    def set_key(self,key):
        super().__init__(key)
        # print(self.x,self.y)
    
    def get_level(self):
        return self.level
    
    def reset_level(self):
        self.level = 0
    
    def level_up(self):
        self.level+=1

    def __eq__(self, __o: object) -> bool:
        if __o == None:
            return False
        if isinstance(__o,str):
            return self.label == __o
        if self.x == None or self.y == None:
            return self.label == __o.label
        return self.label == __o.label and self.x == __o.x and self.y == __o.y
    
    def __format__(self, __format_spec: str) -> str:
        return f"{self.label} in: {self.x},{self.y} type:{self.type}"
    
    def get_type(self):
        return self.type
    
    def set_type(self, _type):
        self.type = _type
    
    def get_position(self):
        return self.x,self.y
    
    def set_position(self,x,y):
        self.x = x
        self.y = y

    def get_label(self):
        return self.label
    
    def set_disk(self,disk):
        if self.type == "normal" :
            return
        self.disk = disk
   
    def set_disk_(self,disk):
        if self.disk == None:
            self.disk = [disk]
        else:
            self.disk.append(disk)

    def get_disk(self):
        return self.disk

class Disk():
    def __init__(self,on_vertex,color):
        self.on_vertex = on_vertex
        self.color = color
        self.x = on_vertex.x
        self.y = on_vertex.y
    
    def __eq__(self, __o: object) -> bool:
        if __o == None:
            return False
        return self.color == __o.color and self.x == __o.x and self.y == __o.y
    
    def __str__(self) -> str:
        return f"{self.color} in: {self.x},{self.y}"
    
    def get_color(self):
        return self.color
    
    def get_position(self):
        return self.x,self.y

    def set_vertex(self,v):
        if v == None:
            self.on_vertex = v
            
        elif v.get_type() == "station":
            self.on_vertex = v

    def get_vertex(self):
        return self.on_vertex

    def get_station(self,v):
        return self.on_vertex
    
class Obstacle():
    def __init__(self,x,y):
        self.x = x
        self.y = y

    def get_position(self):
        return self.x,self.y

    def set_position(self):
        return self.x,self.y

    def __eq__(self, __o: object) -> bool:
        if __o == None:
            return False
        return self.x == __o.x and self.y == __o.y


