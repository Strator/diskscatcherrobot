import math
from controllers.standard import * #type:ignore
from models.virtual_robot import * #type:ignore
from data.geometry import * #type:ignore

class Polar2DControllerRetro:

    def __init__(self, KP_linear, v_max, KP_heading, w_max,inversion_distance):
        self.linear  = PIDSat(KP_linear, 0, 0, v_max) #type:ignore
        self.angular = PIDSat(KP_heading, 0, 0, w_max) #type:ignore
        # self.angular_retro = PIDSat(20,0,0,w_max)
        self.retro =False
        self.inversion_distance = inversion_distance
    def evaluate(self, delta_t, xt, yt ,current_pose):
        (x, y, theta) = current_pose

        dx = xt - x
        dy = yt - y

        target_heading = math.atan2(dy , dx)

        self.target_heading = target_heading
        
        distance = math.sqrt(dx*dx + dy*dy)
        heading_error = normalize_angle(target_heading - theta) #type:ignore
        
        if ((heading_error > math.pi/2 )or(heading_error < -math.pi/2 )) and distance < self.inversion_distance : 
            distance = -distance
            heading_error = normalize_angle(heading_error +  math.pi) #type:ignore


        v_target = self.linear.evaluate_error(delta_t, distance)
        w_target = self.angular.evaluate_error(delta_t, heading_error ) 
        
        return (v_target, w_target)