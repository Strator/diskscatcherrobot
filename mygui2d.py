import sys


from PyQt5 import QtGui, QtCore
from PyQt5.QtWidgets import QApplication, QWidget

# XPADDING = 50
# YPADDING = 500 
# RESOLUTION = 100
def adjustedCoordinate(x,y,d,geometry,padding,resolution):

    x_pos = (padding[0] + x * resolution ) - (d[0] / 2)
    y_pos = (geometry[3] - (2 * padding[1])  -  y * resolution ) - (d[1] / 2)
    
    return int(x_pos),int(y_pos)

class CartWindow(QWidget):


    def __init__(self, *args, _title = "Robot 2D Simulator", _geometry = (0,0,1000,600), _padding = (50,50,50,50), _resolution = 100 , _delta= 1e-4):
        super(CartWindow, self).__init__()
        self.args = args
        self._padding =  _padding
        self.resolution = _resolution
        self.delta_t = _delta
        self.initUI(_title,_geometry)

    def initUI(self,_title,_geometry):
        self.setGeometry(*_geometry)
        self.setWindowTitle(_title)
        self.show()

        for arg in self.args:
            arg.initUI()
       
        self._timer_painter = QtCore.QTimer(self)
        self._timer_painter.start(self.delta_t * 1000)
        self._timer_painter.timeout.connect(self.go)


    def go(self):
        for arg in self.args:
            if not(arg.step()):
                self._timer_painter.stop()
        self.update() # repaint window

    def drawFrame(self,qp):
                #p1x #p2y  #p2x  #p2y     
        padding = self._padding
        _geometry = self.geometry().getRect()
        
        # print(padding[0], _geometry[3] - 2*padding[1])
        #bottomLine
        qp.drawLine(padding[0], _geometry[3] - 2*padding[1], _geometry[2] - 2*padding[0], _geometry[3] - 2 * padding[1])
        
        #leftLine
        qp.drawLine(padding[0], _geometry[3] - 2*padding[1], padding[0], padding[1])
        
        #upperLine
        qp.drawLine(padding[0], padding[1], _geometry[2] - 2*padding[0], padding[2])
        
        #rightLine
        qp.drawLine(_geometry[2] - 2*padding[2], padding[3], _geometry[2]- 2*padding[2], _geometry[3]- 2*padding[3])

    def paintEvent(self, event):
        
        qp = QtGui.QPainter(self)
        
        # qp.begin(self)
        # qp.drawRect(event.rect())
 
        qp.setPen(QtCore.Qt.black)

        self.drawFrame(qp)
        # qp.end()
        
        del qp

        for arg in self.args:
            arg.paint(self,self._padding,self.geometry().getRect(),self.resolution)
      
        

def main():

    app = QtGui.QApplication(sys.argv)
    ex = MainWindow() #type:ignore
    sys.exit(app.exec_())


if __name__ == '__main__':
    main()
