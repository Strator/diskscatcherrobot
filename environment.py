import sys
from time import sleep
from mygraph import IsTypeOf, Graph, IsEqualTo, isOneOf
sys.path.append("./lib")
import math
from random import * 
from PyQt5.QtGui import QPainter, QTransform
from PyQt5 import QtCore
from phidias.Agent import *
from phidias.Globals import *
from phidias.Lib import *
from phidias.Types import *
from iPhidias.phidias_interface import * #type:ignore
from objects import Disk, Obstacle, Vertex
from mygui2d import adjustedCoordinate

import numpy as np
# seed(123)
#believis

#presenza blocco su un vertice del grafo di navigazione
class disk(Belief):pass

#richiesta attivazione sensore
class sense_disk(Belief):pass

#presenza collegamento tra due vertici
class link(Belief):pass

#posizione di un vertice
class position(Belief):pass

#presenza ostacolo su un vertice del grafo di navigazione
class obstacle(Belief):pass

#presenza di una stazione su un vertice del grafo di navigazione
class station(Belief):pass

#richiesta rimozione blocco
class remove_disk(Belief):pass

#stato robot raggiunta del vertice destinazione dopo attuazione path planning
class target_reached(SingletonBelief):pass

#settaggio percorso
class set_path(Belief):pass

#avvio navigazione robot
class start(Belief):pass

#rimozione statione dal grafo di navigazione
class remove_station(Belief):pass

class red_disks(SingletonBelief):pass
class green_disks(SingletonBelief):pass
class blue_disks(SingletonBelief):pass

#percorso minimo in corso
class min_path(SingletonBelief):pass
#rimozione link
class remove_link(SingletonBelief):pass
#presa blocco
class pick(SingletonBelief):pass
#stato serbatoio
class serbatoio(SingletonBelief):pass
#destinazione corrente
class current_destination(SingletonBelief):pass
#semaforo per gestire la scarica dei dischi
class in_scarica(SingletonBelief):pass
#stazione vuota
class empty(Belief):pass
#piazza il blocco nel garage
class put_disk(Belief):pass

#output sensore colore
class color (SingletonBelief):pass

#crea il garage
class set_garage(Belief):pass

#posizioni dei garage
class red_garage(Belief):pass
class blue_garage(Belief):pass
class green_garage(Belief):pass


#procedures
class dijkstra(Procedure):pass
class gen_obstacle(Procedure):pass
class pick_disk(Procedure): pass
class gen_station(Procedure):pass
class init_path(Procedure):pass
class start_navigate(Procedure):pass
class gen_disk(Procedure): pass
class current_position(SingletonBelief):pass
class scarica(Procedure):pass
class gen_garage(Procedure):pass
class _scarica(Procedure):pass
class _next(Procedure):pass
class station_visited(SingletonBelief):pass
class gen_targets(Procedure):pass

def_vars("N","V","U","C","X","Y","Z","M","L","G")
NSTATION = 20
NOBSTACLE = 5
NDISK = 10
class Environment:
    def __init__(self,mediator):
        #grafo dei cammini
        self.fixedGraph = Graph() 
        #mediatore per la comunicazione
        self.mediator = mediator
        #lista dei nodi stazione
        self.stations = list()
        #lista dei blocchi
        self.disks = list()
        #lista degli ostacoli
        self.obstacles = list()
        #lista depositi
        self.garages = list()
        #lista visitati
        self.visited = list()

    def add_link(self,terms):
        self.fixedGraph.link(terms[0],terms[1],terms[2])
    
    def set_position(self,terms):
        self.fixedGraph.setPosition(terms[0],terms[1],terms[2])

    def set_station(self,terms):
        edges = list(self.fixedGraph.edges) 
        shuffle(edges)
        while True:
            v = edges.pop(0)
            i = [0,1]
            shuffle(i)
            v = v[i[0]]
            if v.get_type() == "normal":
                v.set_type("station")
                self.stations.append(v)
                self.mediator.send_belief("Environment_Agent@127.0.0.1:6666","station",v.label,"robot")
                break
        
    def set_garage(self,terms):
        v = self.fixedGraph.vertices[terms[0]]
        if v not in (self.garages + self.stations)  and v.get_disk() == None:
            v.set_type(terms[1])
            self.garages.append(v)
            self.mediator.send_belief("Environment_Agent@127.0.0.1:6666",terms[1],v.label,"robot")

    def set_disk(self,terms):
        stations = list(self.stations)
        shuffle(stations)
        colors = ["red","blue","green"]
        shuffle(colors)
        color = colors[0]
        while stations :
            s = stations.pop(0)
            if s.get_disk() == None:
                d = Disk(s,color)
                s.set_disk(d)
                self.disks.append(d) 
                self.mediator.send_belief("Environment_Agent@127.0.0.1:6666","disk",[s.label,color],"robot")
                break

    def remove_disk(self,terms):
        for i,d in enumerate(self.disks):
            v = d.get_vertex() 
            if v.label == terms[0]:
                v.set_disk(None)
                d.set_vertex(None)
                v.set_type("normal")
                self.stations.remove(v)
                if v in self.fixedGraph.visited_station:
                    self.fixedGraph.visited_station.remove(v)
                self.disks.pop(i)
                break
    
    def add_obstacle(self,terms):
        edges = list(self.fixedGraph.edges)
        shuffle(edges)
        while True:
            e = edges.pop(0)
            o_x = (e[0].x + e[1].x) / 2
            o_y = (e[0].y + e[1].y) / 2
            o = Obstacle(o_x,o_y)
            if o not in self.obstacles:
                self.obstacles.append(o)
                self.fixedGraph.edges.pop(self.fixedGraph.edges.index(e))
                self.mediator.send_belief("Environment_Agent@127.0.0.1:6666","remove_link",(e[0].label,e[1].label),"robot")
                self.mediator.send_belief("Environment_Agent@127.0.0.1:6666","obstacle",(o.x,o.y),"robot")
                break
        
    def min_path(self,terms):
        path = []
        if terms[1] == "station":
            path = self.fixedGraph.dijkstra(terms[0],IsTypeOf(self.fixedGraph.vertices[terms[0]],terms[1],len(self.stations)))
        elif terms[1] in ["red_garage","green_garage","blue_garage"]:
            path = self.fixedGraph.dijkstra(terms[0],IsTypeOf(self.fixedGraph.vertices[terms[0]],terms[1]))  
        elif isinstance(terms[1],list):
            if terms[0] in terms[1]:
                alredy_arrived =  [self.fixedGraph.vertices[terms[0]]]
                self.mediator.send_belief("Robot_Agent@127.0.0.1:6666","target_reached",[ [vertex.get_position() for vertex in alredy_arrived]],"robot")
            else:    
                path = self.fixedGraph.dijkstra(terms[0],isOneOf(terms[1]))
        else:
            path = self.fixedGraph.dijkstra(terms[0],IsEqualTo(terms[1]))
        
        if path:
            self.mediator.send_belief("Robot_Agent@127.0.0.1:6666","min_path",[ [vertex.get_position() for vertex in path]],"robot")
            self.mediator.send_belief("Robot_Agent@127.0.0.1:6666","current_destination",[path[-1].label],"robot")
        
    def sense_disk(self,terms):
       s = self.fixedGraph.vertices[terms[0]]
       if math.hypot(s.x-terms[1],s.y-terms[2]) < 0.1:
            if s.get_disk():
                # self.mediator.send_belief("Robot_Agent@127.0.0.1:6666","color",[s.get_disk().get_color()],"robot")
                self.mediator.send_belief("Robot_Agent@127.0.0.1:6666","disk",[s.label,s.get_disk().get_color()],"robot")
            else:
                self.mediator.send_belief("Robot_Agent@127.0.0.1:6666","empty",[s.label],"robot")
    
    def put_disk(self,terms):
        garage = self.fixedGraph.vertices[terms[0]]
        disk = Disk(garage,terms[1])
        disk.x += random() / 10
        disk.y += random() / 10
        garage.set_disk_(disk)

    def on_belief(self,_from,name,terms):
        agent_name =_from.split('@')[0]

        Environment_functions ={
            "link" : self.add_link,
            "position" : self.set_position,
            "station" : self.set_station,
            "disk" : self.set_disk,
            "remove_disk": self.remove_disk,
            "obstacle": self.add_obstacle,
            "set_garage":self.set_garage,
            
        }

        Robot_functions = {
            "min_path": self.min_path,
            "sense_disk": self.sense_disk,
            "put_disk":self.put_disk
        }

        if agent_name == "Environment_Agent":
            Environment_functions[name](terms)    

        if agent_name == "Robot_Agent":
            Robot_functions[name](terms)
    
    def initUI(self):
        pass
    
    def run(self):
        return True

    def paint(self,window,padding,geometry,resolution):
        qp = QPainter(window)
        d=50

        try:
            for e in self.fixedGraph.edges:
                qp.setPen(QtCore.Qt.darkMagenta)
                v = e[0]
                u = e[1]
                (v_x_pos,v_y_pos) = adjustedCoordinate(v.x,v.y,(d,d),geometry,padding,resolution)
                (u_x_pos,u_y_pos) = adjustedCoordinate(u.x,u.y,(d,d),geometry,padding,resolution)
                
                qp.drawText(v_x_pos,v_y_pos,v.label)
                qp.drawText(u_x_pos,u_y_pos,u.label)
                
                qp.drawText(v_x_pos + 10,v_y_pos,v.get_type())
                qp.drawText(u_x_pos + 10,u_y_pos,u.get_type())

                qp.drawEllipse(v_x_pos,v_y_pos,d,d)
                qp.drawEllipse(u_x_pos,u_y_pos,d,d)
                
                qp.setPen(QtCore.Qt.black)
                qp.drawLine(v_x_pos + d/2 ,v_y_pos + d/2 ,u_x_pos + d/2 ,u_y_pos + d/2)
        except:
            pass
   
        for o in self.obstacles:

            qp.setPen(QtCore.Qt.black)
            (x,y) = o.get_position()
            (x_pos,y_pos) = adjustedCoordinate(x,y,(d/2,d/2),geometry,padding,resolution)
            qp.setBrush(QtCore.Qt.black)
            qp.drawRect(x_pos,y_pos,d/2,d/2)

        colors = {"red":QtCore.Qt.red, "blue":QtCore.Qt.blue, "green":QtCore.Qt.green}

        for disk in self.disks:
            qp.setPen(QtCore.Qt.black)
            qp.setBrush(colors[disk.get_color()])
            (x,y) = disk.get_position()
            (x_s_pos,y_s_pos) = adjustedCoordinate(x,y,(d/2,d/2),geometry,padding,resolution)
            qp.drawEllipse(x_s_pos,y_s_pos,d/2,d/2)
        
        for garage in self.garages:
            qp.setPen(colors[garage.get_type().split("_")[0]])
            qp.setBrush(QtCore.Qt.white)
            (x,y) = garage.get_position()
            (x_s_pos,y_s_pos) = adjustedCoordinate(x,y,(d,d),geometry,padding,resolution)
            qp.drawEllipse(x_s_pos,y_s_pos,d,d)

            if garage.get_disk():
                for gd in garage.get_disk():
                    qp.setPen(QtCore.Qt.black)
                    qp.setBrush(colors[garage.get_type().split("_")[0]])
                    (x_gd,y_gd) = gd.get_position()
                    (gd_x_s_pos,gd_y_s_pos) = adjustedCoordinate(x_gd,y_gd,(d/2,d/2),geometry,padding,resolution)
                    qp.drawEllipse(gd_x_s_pos,gd_y_s_pos,d/2,d/2)
    
        
        qp.end()
       
    def step(self):
        return True

class Environment_Agent(Agent):

    def main(self):
        mediator = {"to":"robot@127.0.0.1:6565"}
        Rob_Agent = {"to":"Robot_Agent@127.0.0.1:6666"} 
        
        +link(V,U,C) >> [+link("Environment",V,U,C)[mediator]] #type:ignore
        +position(U,X,Y) >> [+position("Environment",U,X,Y)[mediator]]#type:ignore
        +position(U,X,Y) / (position(N,X,Y) & neq(U,N)) >> [ -position(N,X,Y)] #type:ignore

        gen_station(0) >> []
        gen_station(N) >> [+station("Environment")[mediator],"N = N - 1", gen_station(N)] #type:ignore

        gen_disk(0) >> []
        gen_disk(N) >> [+disk("Environment")[mediator],"N = N - 1", gen_disk(N)] #type:ignore

        gen_obstacle(0) >> []
        gen_obstacle(N) >> [+obstacle("Environment")[mediator],"N = N - 1", gen_obstacle(N)] #type:ignore

        gen_obstacle() >> [gen_obstacle(NOBSTACLE)]        
        gen_station() >> [gen_station(NSTATION)]
        gen_disk() >> [gen_disk(NDISK)]

      
        gen_garage(V,X) >> [+set_garage("Environment",V,X)[mediator]] #type:ignore

        +remove_disk(U)[{"from":Z}] >> [+remove_disk("Environment",U)[mediator], -disk(U), -remove_disk(U)] #type:ignore
        # +remove_station(U)[{"from":Z}] >> [+remove_station("Environment",U)[mediator], -station(U), -remove_station(U)]
        +remove_link(U,V) [{"from":Z}] / link(U,V,X)>> [-link(U,V,X)] #type:ignore

class gts (ActiveBelief):
    def evaluate(self,*args):
        sum = 0
        for arg in args[:-1]:
            sum += arg()
      
        return sum > args[-1]()
class notIn(ActiveBelief):
    def evaluate(self, *args):
        return args[1]() not in args[0]()
    
class sensed_disk(Belief):pass
class picked_disk(Belief):pass

class isIn(ActiveBelief):
    def evaluate(self, *args):
        if len(args[0]()) > 1:
            return False
        return args[1]() in args[0]()

mediator = {"to":"robot@127.0.0.1:6565"}
Env_Agent = {"to":"Environment_Agent@127.0.0.1:6666"}

class Robot_Agent(Agent):
    def navigation_system(self):
        
        dijkstra(V,U) >> [+min_path("Environment",V,U)[mediator]] #type:ignore
        init_path() / min_path(V) >> [+set_path("Robot",V)[mediator]] #type:ignore
        
        start_navigate(U) / current_destination(V) >> [dijkstra(V,U),init_path(),+start("Robot")[mediator]] #type:ignore 
        start_navigate(U) >> [dijkstra("a",U),init_path(),+start("Robot")[mediator]] #type:ignore
        start_navigate(V,U) >> [dijkstra(V,U),init_path(),+start("Robot")[mediator]] #type:ignore
    
    def target_reached(self):
        
         # se la destinazione è a lavoro finito
        +target_reached(X,Y)[{"from":Z}] / current_destination('a') >> [ +current_position('a') ,show_line("Fine")] #type:ignore

        # se station_Visited è minore di NSTATION siamo in modalità scansione, aggiungiamo il disco e andiamo alla prossima stazione
        +target_reached(X,Y)[{"from":Z}] / (current_destination(U) & station_visited(N) & leq(N,NSTATION)) >> [  "N = N+1",+station_visited(N), +current_position(U), +sense_disk("Environment",U,X,Y)[mediator]] #type:ignore

        # se il target è un garage
        +target_reached(X,Y)[{"from":Z}] / (current_destination(U) & in_scarica(C)) >> [ +current_position(U), _scarica(U,C),show_line("scarico") ]#type:ignore
        
        # se non siamo in visita delle stazioni generiamo i target iniziamo a navigare verso i target e 
        +target_reached(X,Y)[{"from":Z}] / (current_destination(U)) >> [  +current_position(U), +sense_disk("Environment",U,X,Y)[mediator],show_line("Prendo i dischi:")] #type:ignore
       
    def sensing(self):

        #durante la fase di esplorazione
        +disk(V,C) [{"from":Z}] / (station_visited(X) & leq(X,NSTATION)) >> [-disk(V,C), +sensed_disk(V,C), show_line("Sensed: ",C),_next()] #type:ignore
        +empty(V) [{"from":Z}] / (station_visited(X) & leq(X,NSTATION)) >> [-empty(V),show_line("sensed: Empty"),_next()] #type:ignore
        #####
        
        #durante la fase di cattura
        +disk(V,C) [{"from":Z}] / (station_visited(G) & gt(G,NSTATION) &  eq(C,"red")) >> [pick_disk(V,C)]     #type:ignore
        
        +disk(V,C) [{"from":Z}] / (station_visited(G) & gt(G,NSTATION) &  eq(C,"blue") & sensed_disk(X,Y) & eq(Y,"red")) >> [_next(),show_line("sensed: ",C," non è il suo turno")]     #type:ignore
        +disk(V,C) [{"from":Z}] / (station_visited(G) & gt(G,NSTATION) &  eq(C,"green") & sensed_disk(X,Y) & eq(Y,"red")) >> [_next(),show_line("sensed:",C," non è il suo turno")]     #type:ignore
        
        +disk(V,C) [{"from":Z}] / (station_visited(G) & gt(G,NSTATION) &  eq(C,"blue") ) >> [pick_disk(V,C)]  #type:ignore
        
        +disk(V,C) [{"from":Z}] / (station_visited(G) & gt(G,NSTATION) &  eq(C,"green") & sensed_disk(X,Y) & eq(Y,"blue")) >> [_next(),show_line("sensed",C,"non è il suo turno")]     #type:ignore
        
        +disk(V,C) [{"from":Z}] / (station_visited(G) & gt(G,NSTATION) &  eq(C,"green")) >> [pick_disk(V,C)]  #type:ignore

        +picked_disk(X,C) / (serbatoio(N) & lt(N,3)) >> [_next()] #type:ignore

    def gen_targets(self,color):
        gen_targets() / (current_position(U) & sensed_disk(X,C) & eq(C,color)) >> [gen_targets(U,[],C)] #type:ignore
        gen_targets(U,X,C) / (sensed_disk(Y,Z) & notIn(X,Y) & eq(Z,color)) >> ["X.append(Y)", gen_targets(U,X,Z)] #type:ignore
        gen_targets(U,X,Z) / eq(Z,color) >> [start_navigate(U,X), show_line("target generati: ",color,X), "X.clear()"] #type:ignore

    def acting(self):
        pick_disk(U,C) / (serbatoio(X) & lt(X,3)) >> [ #type:ignore
            "X = X+1",
            -disk(U,C), #type:ignore
            -sensed_disk(U,C),  #type:ignore
            +serbatoio(X), #type:ignore
            +picked_disk(U,C),  #type:ignore
            +disk("Robot",U)[mediator],  #type:ignore
            +remove_disk(U) [Env_Agent] #type:ignore
            ] #type:ignore
    
    def _scarica(self,color):
        garage = color + "_garage"
        scarica() / ( current_position(U) & picked_disk(X,C) & eq(C,color)) >> [start_navigate(U,garage), +in_scarica(C)] #type:ignore

    def put_down(self):

        _scarica(U,C) / (serbatoio(X) & gt(X,0) & picked_disk(Y,Z) & eq(C,Z)) >> [ "X = X-1", #type:ignore
        
         +serbatoio(X), #type:ignore
        
         -picked_disk(Y,Z), #type:ignore
        
         +put_disk("Environment",U,C)[mediator], #type:ignore
        
         +disk(U,C,"garage")[Env_Agent], #type:ignore
        
         +remove_disk("Robot",Y)[mediator], #type:ignore
        
         _scarica(U,C)] #type:ignore
        
        _scarica(U,C) / (serbatoio(X) & gt(X,0) & picked_disk(Y,Z)) >> [scarica()] #type:ignore
        _scarica(U,C) / (serbatoio(X) & eq(X,0)) >> [-in_scarica(C),_next()] #type:ignore

    def update_serbatoio(self):

        +serbatoio(X) / eq(X,3) >> [scarica()]  #type:ignore
        +serbatoio(X) / sensed_disk(Y,C) >> [show_line("posti serbatoio: ", X)] #type:ignore
        +serbatoio(X) / in_scarica(C) >> [show_line("scaricamento")] #type:ignore
        +serbatoio(X) / picked_disk(Y,C)  >> [scarica(), show_line("ultima scarica")] #type:ignore

    def next_strategy(self):
        _next() / (current_position(U) & station_visited(N) & lt(N,NSTATION)) >> [ start_navigate(U,"station"),show_line("vado alla prossima stazione")] #type:ignore
        _next() / in_scarica(L) >> [] #type:ignore
        _next() / (sensed_disk(X,Y)) >> [gen_targets()] #type:ignore
        _next() / current_position(U) >> [start_navigate(U,"a")] #type:ignore

    def main(self):
        
        
        self.navigation_system()

        self.target_reached()

        self.sensing()

        self.gen_targets("red")
        self.gen_targets("blue")
        self.gen_targets("green")

        self.acting()

        self._scarica("red")
        self._scarica("blue")
        self._scarica("green")

        self.update_serbatoio()
        
        self.put_down()

        self.next_strategy()
        


        

if __name__ == "__main__":
    

    agent = Environment_Agent()
    agent.start()
    
    robot = Robot_Agent()
    robot.start()
    
    PHIDIAS.run_net(globals(),"http",port=6666)
    node_names = list("bcdefghikjlmnopqrstuvxwyz")
   
    positions = dict()
    for i in np.linspace(1,7.5,5):
        for j in np.linspace(0.5,4,5):
            v  = node_names.pop(0)
            PHIDIAS.assert_belief(position(v,i,j),"Environment_Agent")
            positions[v] = (i,j)
    
    node_names = list("bcdefghikjlmnopqrstuvxwyz")

    for i in range(25):
        # if i+1 % 5 == 0:
            # continue
            
        p1 = positions[node_names[i]] 
        if (i+1) < 25 and (i+1) % 5 > 0:
            p2 = positions[node_names[i+1]] 
            dist_ = math.hypot(p1[0] - p2[0], p1[1] - p2[1])
            PHIDIAS.assert_belief(link(node_names[i],node_names[i+1],dist_),"Environment_Agent")
        if i+5 < 25:
            p2 = positions[node_names[i+5]] 
            dist_ = math.hypot(p1[0] - p2[0], p1[1] - p2[1])
            PHIDIAS.assert_belief(link(node_names[i],node_names[i+5],dist_),"Environment_Agent")
    
    # PHIDIAS.assert_belief(red_disks(0),"Robot_Agent")
    # PHIDIAS.assert_belief(blue_disks(0),"Robot_Agent")
    # PHIDIAS.assert_belief(green_disks(0),"Robot_Agent")
    
    # PHIDIAS.assert_belief(red_disks(0),"Environment_Agent")
    # PHIDIAS.assert_belief(blue_disks(0),"Environment_Agent")
    # PHIDIAS.assert_belief(green_disks(0),"Environment_Agent")

    PHIDIAS.assert_belief(station_visited(0),"Robot_Agent")    


    PHIDIAS.assert_belief(position("a",0,0),"Environment_Agent")
    
    p1 = (0,0)
    p2 = positions["b"]
    PHIDIAS.assert_belief(link("a","b",math.hypot(p1[0]-p2[0],p1[1]-p2[1])),"Environment_Agent")
    p2 = positions["c"]
    PHIDIAS.assert_belief(link("a","c",math.hypot(p1[0]-p2[0],p1[1]-p2[1])),"Environment_Agent")
    p2 = positions["d"]
    PHIDIAS.assert_belief(link("a","d",math.hypot(p1[0]-p2[0],p1[1]-p2[1])),"Environment_Agent")
    p2 = positions["e"]
    PHIDIAS.assert_belief(link("a","e",math.hypot(p1[0]-p2[0],p1[1]-p2[1])),"Environment_Agent")
    p2 = positions["f"]
    PHIDIAS.assert_belief(link("a","f",math.hypot(p1[0]-p2[0],p1[1]-p2[1])),"Environment_Agent")
    
    PHIDIAS.assert_belief(position("g1",3.42,0),"Environment_Agent")
    PHIDIAS.assert_belief(position("g2",5.07,0),"Environment_Agent")
    PHIDIAS.assert_belief(position("g3",6.68,0),"Environment_Agent")
    
    p1 = (3.42,0)
    p2 = positions["g"]
    PHIDIAS.assert_belief(link("g1","g",math.hypot(p1[0]-p2[0],p1[1]-p2[1])),"Environment_Agent")
    p2 = positions["l"]
    PHIDIAS.assert_belief(link("g1","l",math.hypot(p1[0]-p2[0],p1[1]-p2[1])),"Environment_Agent")

    p1 = (5.07,0)
    p2 = positions["l"] 
    PHIDIAS.assert_belief(link("g2","l",math.hypot(p1[0]-p2[0],p1[1]-p2[1])),"Environment_Agent")
    p2 = positions["q"]
    PHIDIAS.assert_belief(link("g2","q",math.hypot(p1[0]-p2[0],p1[1]-p2[1])),"Environment_Agent")

    p1 = (6.68,0)
    p2 = positions["q"]
    PHIDIAS.assert_belief(link("g3","q",math.hypot(p1[0]-p2[0],p1[1]-p2[1])),"Environment_Agent")
    p2 = positions["v"]
    PHIDIAS.assert_belief(link("g3","v",math.hypot(p1[0]-p2[0],p1[1]-p2[1])),"Environment_Agent")
    

    PHIDIAS.achieve(gen_garage("g1","red_garage"),"Environment_Agent")
    PHIDIAS.achieve(gen_garage("g2","blue_garage"),"Environment_Agent")    
    PHIDIAS.achieve(gen_garage("g3","green_garage"),"Environment_Agent")
    
    PHIDIAS.achieve(gen_obstacle(),"Environment_Agent")
    PHIDIAS.achieve(gen_station(),"Environment_Agent")
    PHIDIAS.achieve(gen_disk(),"Environment_Agent")
    # PHIDIAS.achieve(gen_disk(),"Environment_Agent")
    
    PHIDIAS.assert_belief(serbatoio(0),"Robot_Agent")
    sleep(5)
    PHIDIAS.achieve(start_navigate("station"),"Robot_Agent")
    # PHIDIAS.achieve(dijkstra("a","station"),"Robot_Agent")

    # PHIDIAS.achieve(init_path(),"Robot_Agent")
    # PHIDIAS.run_net(globals(),"http",port=6667)

    PHIDIAS.shell(globals(),"Robot_Agent")