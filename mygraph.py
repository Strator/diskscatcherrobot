from cmath import inf
from errno import EDEADLK
from fibheap import *
from objects import Vertex

class Graph:
    
    def __init__(self,vertices=dict(),edges=list(),costs=dict(),getVertex=dict(),getValue=dict()):
        
        if not isinstance(vertices,dict) or not isinstance(edges,list):
            print("Errore argomenti costrutture non validi: vertices=list(), edges=list(), cost=dict() ")
            return

        self.vertices = vertices
        self.edges = edges
        self.costs = costs
        self.getVertex = getVertex
        self.getValue = getValue
        self.visited_station = []

    def link (self,u,v,cost=1):
        if cost < 0:
            print("Errore il costo non può essere negativo")
            return 

        try :
            u = self.vertices[u]
        except:
            u = Vertex(u)
            self.vertices[u.label] = u
        try :
            v = self.vertices[v]
        except:
            v =  Vertex(v)
            self.vertices[v.label] = v  

        self.edges.append((u,v))
        self.costs[(u.label,v.label)] = cost
        
    def setPosition(self,label,x,y):
        try:
            self.vertices[label].set_position(x,y)
        except:
            v = Vertex(label,x=x,y=y)
            self.vertices[label] = v    
    def _cost(self,u,v):
        try:
            return self.costs[(u.label,v.label)]
        except:
           return self.costs[(v.label,u.label)]

    def init_distance(self):
        fheap = makefheap()
        self.dist = dict()
        
        #inserisce i vertici nell'heap ed indicizza i nodi dell'heap con i vertici del grafo, inizializza anche la distanza.
        for v_label,v in self.vertices.items():
            v.set_key(inf)
            fheap.insert(v)
            self.dist[v_label] = inf

        return fheap
    
    def dijkstra(self,source,postCondition):
        fheap = self.init_distance()
        source = self.vertices[source]
        fheap.decrease_key(source,0)
        
        self.dist[source.label] = 0
        parents = dict()

        current_source = fheap.extract_min()
        
        while fheap.minimum():
            
            # print(current_source.label)
            #se il criterio di arresto è vero ci fermiamo.
            if postCondition.evaluate(current_source,self.visited_station):
                del fheap
                break            
            # se abbiamo estratto un nodo con distanza infinito significa che il nodo è irragiungibile e ci fermiamo
            if self.dist[current_source.label] == inf:
                del fheap
                return list()
            
            edges = list(iter(zip(*self.edges)))
            left_neighbors = [edges[0][i] for i,v in enumerate(edges[1]) if v == current_source]
            
            right_neighbors = [edges[1][i] for i,v in enumerate(edges[0]) if v == current_source]

            neighbors = left_neighbors + right_neighbors
            # print(list(map(lambda x: x.label,neighbors)))
          
            # prende i vicini del vertice corrente, aggiorna la stima, e aggiorna il vettore parent.
            for adj in neighbors: #self.edges[self.getValue[current_source]]: 
                alt = self.dist[current_source.label] + self._cost(adj,current_source)
          
                if alt < self.dist[adj.label]:
          
                    self.dist[adj.label] = alt
                    fheap.decrease_key(adj,alt)
                    parents[adj.label] = current_source  
            
            current_source = fheap.extract_min()

        #crea un percorso partendo dalla fine del vettore parent, arrivando fino alla sorgente 
        path = []
        
        # print([ (x , y.label) for x,y in parents.items()])
        u = current_source
        # print(u.label)
        while True:
            try:
                if u == source:
                   break
                path.insert(0,u)
                u=parents[u.label]
            except:
                break
        # print([x.label for x in path])
        return path
        
class PostCondition:
    # def __init__(self):
    #     pass
    def evaluate(self,x):
        pass
class isOneOf(PostCondition):
    def __init__(self,_list):
        self._list = _list
    
    def evaluate(self, x, _ ):
        return x in self._list

class IsTypeOf(PostCondition):
    def __init__(self,y,type,n_station=None):
        super().__init__()
        self.type = type
        self.y = y
        self.n_station = n_station
    
    def _evaluate(self, x):
        return x.get_type() == self.type

    def evaluate(self, x,visited_station):
        if self.type in ["red_garage","blue_garage","green_garage"]:
            return self._evaluate(x)

        if len(visited_station) == self.n_station:
            visited_station.clear()

        if x.get_type() == self.type and x not in visited_station and self.y != x:
            visited_station.append(x)
            # print(visited_station)
            return True
        else:
            return False 

class IsEqualTo(PostCondition):
    def __init__(self,y):
        self.y = y
    def evaluate(self, x,_):
        return x == self.y 

def test():
    g = Graph()

    g.link("a","b",1 )

    g.link("a","e",2)

    g.link("b","c",3)

    g.link("e","c",4)
    g.link("c","f",3)
    g.link("c","d",3)
    g.link("a","d",1)
    g.link("f","d",2)
   
   
    print(list(map(lambda x: x.label,g.dijkstra("f",IsEqualTo(Vertex("b"))))))
    
    print(list(map(lambda x: x.label,g.dijkstra("b",IsEqualTo(Vertex("f"))))))

    print(list(map(lambda x: x.label,g.dijkstra("f",IsEqualTo(Vertex("b"))))))


if __name__ == "__main__":
    test()