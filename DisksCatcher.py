import sys

from objects import Vertex
#setta la libreria da importare
sys.path.append('./lib/')

import math

from pathlib import Path
from data.plot_window import RealTimeDataPlotter #type:ignore
from models.cart2d import AckermannSteering #type:ignore
from models.robot import RoboticSystem #type:ignore

from controllers.standard import PIDSat #type:ignore
from controllers.control2d import Path2D, Polar2DController #type:ignore
from mycontroller import Polar2DControllerRetro
from mygui2d import CartWindow
from data.plot import * #type:ignore
from data.geometry import normalize_angle #type:ignore
from environment import *


from PyQt5.QtWidgets import QApplication
from PyQt5.QtGui import QPixmap, QPainter, QTransform
import PyQt5.QtCore as QtCore

import mygraph


# import phidias
from phidias.Types import *
from phidias.Agent import *
from phidias.Lib import *
from phidias.Main import *

from iPhidias.phidias_interface import * #type:ignore

class Mediator():
    def __init__(self,port):
        self.collegue = dict()
        start_message_server_http(self,port) #type:ignore

    def register_collegue(self, *args):
        for c in args:
            self.collegue[c[0]] = c[1]
   
    def on_belief(self,_from,name,terms):
        self.collegue[terms[0]].on_belief(_from,name,terms[1:])
       
    def send_belief(self,to,name,terms,_from="robot"):
        Messaging.send_belief(to,name,terms,_from) #type:ignore

    def set_path(self,path):
        print(path)
        self.collegue["robot"].trajectory.set_path(path)
    
    
class TwoEncodersAckerman(AckermannSteering):
    def __init__(self,_M,_b,_L,_R,r_left_encoder,r_right_encoder,_B,_resolution):
        super().__init__(_M,_b,_L,_R)

        self.r_encoder_left = r_left_encoder
        self.r_encoder_right = r_right_encoder
        self.encoder_wheelbase = _B
        self.encoder_resolution = _resolution

        self.theta_r = 0
        self.v_r = 0
        self.w_r = 0
        self.x_r = 0
        self.y_r = 0
    
    def evaluate(self,delta_t,torque,steering_angle):

        super(TwoEncodersAckerman,self).evaluate(delta_t,torque,steering_angle)

        #sensing wheels
        
        vl = self.v - self.w * self.encoder_wheelbase / 2
        vr = self.v + self.w * self.encoder_wheelbase / 2

        #updating sensor
                
        self.delta_rot_left = int((vl / self.r_encoder_left) * (delta_t / self.encoder_resolution)) * self.encoder_resolution
        self.delta_rot_right = int((vr / self.r_encoder_right) * (delta_t / self.encoder_resolution)) * self.encoder_resolution

        #odometry model

        p_left = self.delta_rot_left * self.r_encoder_left
        p_right = self.delta_rot_right * self.r_encoder_right

        self.vleft = p_left / delta_t
        self.vright = p_right / delta_t

        self.v_r = (self.vleft + self.vright) / 2
        self.w_r = (self.vright - self.vleft) / self.encoder_wheelbase

        delta_p = (p_left + p_right) / 2

        delta_theta = (p_right - p_left) / self.encoder_wheelbase

        self.x_r = self.x_r + delta_p * math.cos(self.theta_r + delta_theta / 2)
        self.y_r = self.y_r + delta_p * math.sin(self.theta_r + delta_theta / 2)
        self.theta_r = normalize_angle(self.theta_r + delta_theta)

    def get_pose(self):
        return (self.x_r, self.y_r, self.theta_r)


    def get_speed(self):
        return (self.v_r, self.w_r)

        
class DisksCatcher(RoboticSystem):
    
    SITTING = 0
    MOVING = 1
    TERMINAL = 2
    
    def __init__(self,image_file = "ackermann_robot_2d.png",_scaleToWidth = 40, mediator = None):
        super().__init__(1e-3) # delta_t = 1e-3
        
        current_path = Path(__file__).parent.resolve()
        self.imagePath = str(current_path) + '/lib/icons/' + image_file

        self.scaleToWidth = _scaleToWidth
        # Mass = 20kg - friction = 0.7 - side = 20cm - wheels radius = 3cm
        self.car = TwoEncodersAckerman(20, 0.7, 0.03, 0.2, 0.03, 0.03, 0.3, 1e-4)

        #Controllo in velocità 15 Kp 8 Nm max, antiwindup
        self.speed_controller = PIDSat(35, 10, 0 , 8, True)
        
        #Controllo in Posizione (Polare) della macchina kp = 2, vmax = 3 m/s,# kp = 10, steering max = 45 deg, distanza dal target inversione marcia
        self.polar_controller = Polar2DControllerRetro(10,3,10,math.pi/4,0.4)
        # self.polar_controller = Polar2DController(10,3,10,math.pi/4)
        
        #Generatore traiettoria # vmax = 3 m/s, acc/dec=4, threeshold = 1
        self.trajectory =  Path2D(3,20,20,0.1)

        #status del robot in movimento o fermo
        self.status = DisksCatcher.SITTING
        
        self.mediator = mediator

        self.serbatio = list()
        # (x,y,_) = self.get_pose()

        # self.messageserver = start_message_server_http(self,port=8324)
        # path = [(3,3)]
        # self.trajectory.set_path( path ) 
       
        # self.trajectory.start( (x,y) )
        
        #DataPlotters
        self.dataplot_theta =  RealTimeDataPlotter("Theta vs Theta_Target",["t","time"],[["theta","theta"],["target_theta","target_theta"]],0.5)
        self.dataplot_velocity = RealTimeDataPlotter("V vs Vref",["t","time"],[["vref","vref"],["v","v"],["torque","torque"]],0.5)
        self.dataplot_position = RealTimeDataPlotter("Position vs Position_Target",["t","time"],[["target","target"],["p_target","p_target"],["p","p"]],0.5)
        self.dataplot_steering = RealTimeDataPlotter("Steering vs max_steering",["t","time"],[["steering","steering"],["maxsteering","max_steering"]],0.5)
    
    
    # def set_path(self,path):
    #     self.trajectory.set_path(path)
    
    # def start(self):
    #     self.trajectory.start(self.get_pose()[0:2])

    def on_belief(self,_from,name,terms):
        if name == "set_path":
            if terms[0]:
                self.trajectory.set_path(terms[0])
        if name == "start":
            if self.trajectory.path:
                self.trajectory.start(self.get_pose()[:2])
                self.status = DisksCatcher.MOVING
        if name == "block":
            self.serbatio.append(terms[0])
        if name == "remove_block":
            if terms[0] in self.serbatio:
                self.serbatio.remove(terms[0])
        # pass
    def plotTick(self):
        #Setting new points X
        self.dataplot_theta.add("t",self.t)
        self.dataplot_velocity.add("t",self.t)
        self.dataplot_steering.add("t",self.t)
        self.dataplot_position.add("t",self.t)
   
    def plotUpdate(self):
        #update
        self.dataplot_theta.plot(self.t)
        self.dataplot_velocity.plot(self.t)
        self.dataplot_steering.plot(self.t)
        self.dataplot_position.plot(self.t)
    
    def run(self):

        (x,y,theta) = self.get_pose()

        target  = False

        if self.status == DisksCatcher.MOVING:
            target =  self.trajectory.evaluate(self.delta_t, (x,y,theta))
        
        if target == False and self.status == DisksCatcher.MOVING:
            self.status = DisksCatcher.TERMINAL
        
        
        if self.status == DisksCatcher.TERMINAL:
            self.status = DisksCatcher.SITTING
            self.mediator.send_belief("Robot_Agent@127.0.0.1:6666","target_reached",self.get_pose()[:2],"robot")
            
        if self.status == DisksCatcher.TERMINAL or self.status ==DisksCatcher.SITTING:
            return True
        (x_target, y_target) = target
        
        #genera la vref e lo sterzo da applicare
        (vref, steering) = self.polar_controller.evaluate(self.delta_t,
                                                          x_target, y_target,
                                                          (x,y,theta))
        (v, w) = self.get_speed()
        #calcolo spinta lineare dei motori
        torque = self.speed_controller.evaluate(self.delta_t, vref, v)
        
        self.plotTick()
        
        #Setting new points y
        self.dataplot_theta.add("target_theta",math.degrees(self.polar_controller.target_heading))
        self.dataplot_theta.add("theta",math.degrees(theta))
        
        self.dataplot_steering.add("maxsteering",math.pi/4)
        self.dataplot_steering.add("steering",steering)
        
        self.dataplot_velocity.add("vref",vref)
        self.dataplot_velocity.add("v",v)
        self.dataplot_velocity.add("torque",torque)
        
        self.dataplot_position.add("p_target",math.hypot(x_target,y_target))
        self.dataplot_position.add("target",math.hypot(*self.trajectory.current_target))
        self.dataplot_position.add("p",math.hypot( x,y))
        
        #evaluate stimulus
        self.car.evaluate(self.delta_t, torque, steering)
       
        #update dataplots
        self.plotUpdate()

        return True

    def get_pose(self):
        return self.car.get_pose()

    def get_speed(self):
        return self.car.get_speed()
    
    def initUI(self):
        self.sprite = QPixmap(self.imagePath).scaledToWidth(self.scaleToWidth)

    def paint(self,window,padding,geometry,resolution):
        qp = QPainter(window)
        # qp.begin(window)
        qp.setPen(QtCore.Qt.black)
        # qp.drawRect(event.rect())

        (x,y,theta) = self.get_pose()
        texts  = ["X  = %6.3f m" % (x*10),"Y  = %6.3f m" % (y*10),"Th = %6.3f deg" % (math.degrees(theta)),"T  = %6.3f s" % (self.t) ]
        yposition = range(20,140,20)

        for _y,text in zip(yposition,texts):
            qp.drawText(910,_y,text)
        
        s = self.sprite.size()
   
        x_pos = (padding[0] + x * resolution ) - s.width() / 2
        y_pos = (geometry[3] - (2 * padding[1])  -  y * resolution ) - s.height() / 2
        
        t = QTransform()
        
        t.translate(x_pos + s.width()/2, y_pos + s.height()/2)
        t.rotate(-math.degrees(theta))
        t.translate(-(x_pos + s.width()/2), - (y_pos + s.height()/2))
        
        qp.setTransform(t)
        # print(x_pos,y_pos)
        qp.drawPixmap(int(x_pos),int(y_pos),self.sprite)
        # qp.drawEllipse(*adjustedCoordinate(x-0.1,y-0.1,(10,10),geometry,padding,resolution),10,10)
        
        qp.end()
        
        # print("qua")
        # qp.end() 
        # print("qua")       
        # del qp
        # qp.end()

# class Point:
#     def __init__ (self,x,y,d):
#         self.coordinate = (x,y,d)
#     def initUI(self):
#         pass
#     def paint(self,window,padding,geometry,resolution):
#         qp = QPainter()
        
#         qp.begin(window)
#         qp.setPen(QtCore.Qt.red)
        
#         (x,y,d) = self.coordinate
        
#         xpos = padding[0] + x * resolution - d / 2
#         ypos =  (geometry[3] - (2 * padding[1])  -  y * resolution ) - d / 2
        
#         qp.drawEllipse(int(xpos),int(ypos),d,d)
        
#         qp.end()

#     def step(self):
#         return True
def test():
    app = QApplication(sys.argv)

    mediator  = Mediator(6565)
    env = Environment(mediator)
    start = Vertex("a")
    start.set_type("start")
    env.fixedGraph.vertices["a"] = start
    cart_robot = DisksCatcher(mediator=mediator)
    
    mediator.register_collegue(("Environment",env),("Robot",cart_robot))

    # point = Point(3,3,5)
    ex = CartWindow(env,cart_robot)
    sys.exit(app.exec_())

if __name__ == '__main__':
    test()
    
